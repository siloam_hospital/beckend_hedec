'use strict';
let methods = {};

function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); }

methods.insertBuilder = (request, table, isReturnID) => {
    let obj = request;
    let fields = [];
    let values = [];
    for (var o in obj) {
        if (obj[o] != null) {
            fields.push(o);
            if(isNumber(obj[o])) values.push(obj[o]);
            else if(obj[o] == undefined || obj[o] == null) values.push(null);
            else values.push("'" + obj[o] + "'");
        }
    }
    let query = "INSERT INTO " + table + " (" + fields.join(',') + ") VALUES (" + values.join(',') + ")";
    if (isReturnID) query += "; SELECT SCOPE_IDENTITY()";
    return query;
}

methods.updateBuilder = (table, fields, values, whereParam, whereValue) => {
    let result = [];
    for(let i=0; i<fields.length; i++) {
        if(isNumber(values[i])) result.push(fields[i] + " = "+values[i]);
        else if(values[i] == undefined || values[i] == null) result.push(fields[i] + " = NULL");
        else result.push(fields[i] + " = '"+values[i]+"'");
    }
    let query = "UPDATE " + table + " SET " + result.join(', ') + " WHERE " + whereParam + " = " + whereValue;
    return query;
}

methods.insertQuery = (table, fields, values) => {
    for(let i=0; i<values.length; i++) values[i] = values[i] == null ? "NULL": "'"+values[i]+"'";
    let query = "INSERT INTO " + table + " (" + fields.join(',') + ") VALUES (" + values.join(',') + ")";
    return query;
}

module.exports = methods;
