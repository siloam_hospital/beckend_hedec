const os = require('os');
const { exec } = require('child_process');
const { promisify } = require('util');
const execAsync = promisify(exec);

function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

function sleep(ms = 0) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function avg() {
    let totalIdle = 0, totalTick = 0;
    let cpus = os.cpus();
    cpus.forEach(cpu => {
        for (let type in cpu.times) {
            totalTick += cpu.times[type];
        }
        totalIdle += cpu.times.idle;
    });
    return {
        idle: totalIdle / cpus.length,
        total: totalTick / cpus.length,
    };
}

async function checkHealth(formatted = false, interval = 500) {
    let startMeasure = avg();
    await sleep(interval);
    let endMeasure = avg();
    let idleDifference = endMeasure.idle - startMeasure.idle;
    let totalDifference = endMeasure.total - startMeasure.total;
    let percentageCPU = 100 - ~~(100 * idleDifference / totalDifference);

    let totalMem = os.totalmem();
    let freeMem = os.freemem();
    let usedMem = totalMem - freeMem;

    let uptime = os.uptime();
    return formatted ? {
        percentageCPU: `${percentageCPU}%`,
        usedMem: formatBytes(usedMem),
        freeMem: formatBytes(freeMem),
        totalMem: formatBytes(totalMem),
        uptime: `${~~(uptime / 86400)}d ${~~(uptime / 3600) % 24}h ${~~(uptime / 60) % 60}m ${uptime % 60}s`,
    } : {
        percentageCPU,
        usedMem,
        freeMem,
        totalMem,
        uptime,
    };
}

// async function debugHealth() {
//     let data = await checkHealth(true);
//     console.clear();
//     console.table(data);
// }

/**
 * 
 * @param {'PID' | 'name' | 'cpuPercent' | 'memoryPercent'} sortBy
 * @param {'asc' | 'desc'} direction
 * @param {number} top 
 */
async function getProcessStateList(sortBy = 'cpuPercent', direction = 'desc', top = 5) {
    /**
     * @type {{ PID: number; name: string; cpuPercent: number; memoryPercent: number; diskIO?: number; }[]}
     */
    let processList = [];
    try {
        switch (os.platform()) {
            case 'win32': {
                const totalCPU = os.cpus().length;
                const totalMem = os.totalmem();
                // https://wutils.com/wmi/root/cimv2/win32_perfformatteddata_perfproc_process/
                const cmd = [
                    'Get-WMIObject -class Win32_PerfFormattedData_PerfProc_Process',
                    `Where-Object { $_.name -notlike '_Total' -and $_.name -notlike 'Idle' }`,
                    'Select-Object @(' + [
                        `@{Name='PID'; Expression={$_.IDProcess}}`,
                        `@{Name='name'; Expression={$_.name}}`,
                        `@{Name='cpuPercent'; Expression={$_.PercentProcessorTime / ${totalCPU}}}`,
                        `@{Name='memoryPercent'; Expression={$_.workingSetPrivate / ${totalMem / 100}}}`,
                    ].join() + ')',
                    `Sort-Object ${sortBy} -${direction === 'asc' ? 'Ascending' : 'Descending'}`,
                    `Select -First ${top}`,
                    'ConvertTo-Json',
                ].join(' | ');
                const result = await execAsync(`powershell.exe "${cmd}"`, { windowsHide: true });
                if (result.stdout) {
                    processList = JSON.parse(result.stdout);
                }
                break;
            }
            case 'linux': {
                const template = [
                    `\\"name\\":\\"%s\\"`,
                    `\\"PID\\":%s`,
                    `\\"cpuPercent\\":%s`,
                    `\\"memoryPercent\\":%s`,
                ].join();

                let sorter = 'cpu';
                switch (sortBy) {
                    case 'PID': { sorter = 'pid'; break; }
                    case 'name': { sorter = 'command'; break; }
                    case 'memoryPercent': { sorter = 'pmem'; break; }
                }
                const cmd = [
                    `ps aux --sort=${direction === 'asc' ? '+' : '-'}${sorter}`,
                    `awk 'NR>1'`,
                    `head -n ${top}`,
                    `awk '` + [
                        'BEGIN { ORS = ""; print "[" }',
                        `{ printf "%s{${template}}", separator, $11, $2, $3, $4; separator = ","}`,
                        'END { print "]" }'
                    ].join(' ') + `';`
                ].join(' | ');
                const result = await execAsync(cmd, { windowsHide: true });
                if (result.stdout) {
                    processList = JSON.parse(result.stdout);
                }
                break;
            }
            default: {
                throw new EvalError(`Platform '${os.platform()}' not implemented`);
            }
        }
        return processList.sort((a, b) => {
            let l = a.cpuPercent;
            let r = b.cpuPercent;
            return r - l;
        });
    } catch (e) {
        console.error('State Retrieval failed:', e);
        return [];
    }
}

module.exports = {
    formatBytes,
    checkHealth,
    getProcessStateList,
};
