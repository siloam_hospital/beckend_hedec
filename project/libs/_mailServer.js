'use strict';
let methods = {};
const nodemailer = require("nodemailer");
const pug = require('pug');

const transporter = nodemailer.createTransport({
    host: process.env.smtp_host,
    port: process.env.smtp_port,
    secure: process.env.smtp_secure === "true" ? true : false, // true for 465, false for other ports
    auth: {
        user: process.env.smtp_uname,
        pass: process.env.smtp_pass,
    },
    tls: {
        rejectUnauthorized: process.env.smtp_tls === "true" ? true : false, // do not fail on invalid certs
    },
});
methods.send = function (datamail) {
    return new Promise((resolve, reject) => {
        transporter.verify(async function (error) {
            if (error) {
                reject(error);
            } else {
                let info = await transporter.sendMail({
                    from: process.env.smtp_uname, // sender address
                    to: datamail['to'], // list of receivers
                    cc: (datamail['cc']) ? datamail['cc'] : null,
                    bcc: (datamail['bcc']) ? datamail['bcc'] : null,
                    subject: datamail['subject'], // Subject line
                    text: datamail['text'], // plain text body
                    html: typeof datamail['html'] !== "undefined" ? pug.renderFile(datamail['html']['template'], datamail['html']['data']) : "",
                    attachments: datamail['attachments'],
                });
                if (info.messageId) {
                    resolve(info);
                } else {
                    reject("error send mail");
                }
            }
        });
    })
};

module.exports = methods;