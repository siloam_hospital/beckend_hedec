const mysql = require('mysql2/promise');

class MyDB {
    /**
     * Create new MYSQL DB Connection Instance
     * @param {string | {
     *  host: string;
     *  port: number;
     *  user: string;
     *  password: string;
     *  database?: string;
     *  debug?: boolean;
     *  trace?: boolean;
     *  insecureAuth?: boolean;
     *  localAddress?: string;
     *  socketPath?: string;
     *  charset?: string;
     *  timezone?: string;
     *  connectTimeout?: number;
     *  stringifyObjects?: boolean;
     *  typeCast?: boolean;
     *  supportBigNumbers?: boolean;
     *  bigNumberStrings?: boolean;
     *  dateStrings?: boolean | string[];
     *  localInfile?: boolean;
     *  multipleStatements?: boolean;
     *  flags?: string;
     *  ssl?: { ca?: Buffer, rejectUnathorized?: boolean; }
     * }} connectionOptions 
     */
    constructor(connectionOptions) {
        this.con = connectionOptions;
        this.pool = null;
    }
    /**
     * 
     * @param {string} query 
     * @returns {Promise<any[]>} Query result
     */
    async execute(query) {
        let result;
        let err = null;
        try {
            this.pool = await mysql.createConnection(this.con);
            result = await this.pool.query(query);
        } catch (e) {
            err = e;
        } finally {
            this.pool.end();
        }
        if (err) {
            throw err;
        } else {
            return result[0];
        }
    }
    
    parsePaginate(queryString = '', options = {}) {
        let dataset = {
            page: 0,
            perPage: 0,
            sortBy: '',
            sortDir: '',
            filter: {},
        };
        dataset = {
            page: options._page && parseInt(options._page, 10) > 1 ? options._page : 1,
            perPage: options._perPage || 25,
            sortBy: options._sortBy || null,
            sortDir: options._sortDir || null,
            filter: null,
        };
        Object.keys(options).filter(v => v.indexOf('_like') >= 0).forEach(k => {
            if (!dataset.filter) {
                dataset.filter = {};
            }
            dataset.filter[k.substr(0, k.indexOf('_like'))] = options[k];
        });
        // PAGINATION
        let withPagination = [queryString];
        // 1. Parse filter request
        if (dataset.filter) {
            withPagination.push(...(Object.keys(dataset.filter).map(k => `AND ${k} like '%${dataset.filter[k]}%'`)));
        }
        // 2. Parse sorting with direction
        if (dataset.sortBy) {
            let direction = dataset.sortDir === 'DESC' ? 'DESC' : 'ASC';
            withPagination.push(`ORDER BY ${dataset.sortBy} ${direction}`);
        }
        // 3. Parse paging
        withPagination.push(`LIMIT ${(dataset.page - 1) * dataset.perPage}, ${dataset.perPage}`);
        return withPagination.join(' ');
    }

    countPaginate(queryString = '', options = {}) {
        let dataset = {
            page: 0,
            perPage: 0,
            filter: {},
        };
        dataset = {
            page: options._page && parseInt(options._page, 10) > 1 ? options._page : 1,
            perPage: options._perPage || 25,
            filter: null,
        };
        Object.keys(options).filter(v => v.indexOf('_like') >= 0).forEach(k => {
            if (!dataset.filter) {
                dataset.filter = {};
            }
            dataset.filter[k.substr(0, k.indexOf('_like'))] = options[k];
        });
        // PAGINATION
        let withPagination = [queryString.replace(/SELECT(.|\n)*FROM/gi, 'SELECT count(*) as total FROM')];
        // 1. Parse filter request
        if (dataset.filter) {
            withPagination.push(...(Object.keys(dataset.filter).map(k => `AND ${k} like '%${dataset.filter[k]}%'`)));
        }
        return withPagination.join(' ');
    }

    /**
     * Paginate query
     * @param {string} queryString Target query
     * @param {{ _page: number; _perPage: number; _sortBy: string; _sortDir: 'ASC' | 'DESC'; filter: {[key: string]: any} }} options Query Object (e.g. req.query)
     * @param {string} defaultSortColumn Default sorting column, default to "id"
     * @returns {Promise<{
     *  page: T[];
     *  total: number;
     * }>} Data of paginated result and total count of overall unpaginated rows
     */
    async paginateQuery(queryString = '', options = {}) {
        let r = {
            page: [],
            total: 0,
        };
        let result = await Promise.all([
            this.parsePaginate(queryString, options),
            this.countPaginate(queryString, options),
        ].map(v => this.execute(v)));
        if (result && result[0] && result[1]) {
            r = {
                page: result[0],
                total: result[1][0].total,
            };
            return r;
        } else {
            throw new Error('No Result');
        }
    }
}
exports.MyDB = MyDB;
