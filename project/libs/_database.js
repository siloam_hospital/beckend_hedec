'use strict';
let methods = {};
var Connection = require('tedious').connect;
var Request = require('tedious').Request;
const db_program_name=process.env.Production=="true"?process.env.Service+"_prod":process.env.Service+"_dev";
const timeout=90000;
const config = {
    server: process.env.DB_Host,
    authentication: {
        type: process.env.DB_type,
        options: {
            userName: process.env.DB_userName,
            password: process.env.DB_Password
        }
    },
    options: {
        connectionTimeout: timeout,
        requestTimeout: timeout,
        database: process.env.DB_Name,
        rowCollectionOnRequestCompletion: true,
        connectionRetryInterval:timeout,
        trustServerCertificate:true,
        rowCollectionOnDone:true,
        appName:db_program_name,
        encrypt:true,
        validateBulkLoadParameters:false
    }
}

function readData(connect, query,for1=null) {
    return new Promise((resolve) => {
        let result_data = {
            "message": "",
            "status": false,
            "data": []
        }
        let jsonString = '';
        let forjson = 'FOR JSON AUTO';
        if(for1!=null){
            forjson=for1;
        }
        // console.log('Query Read:', {
        //     query,
        //     forjson,
        //     merged: query + ' ' + forjson,
        // });
        let request = new Request(query + " "+forjson, function (err, rowCount, rows) {
            // connect.close();
            // connect.on("end", async function () {
                
            // })
            connect.close();
            connect.on("end", async function () {
                if(err){
                    result_data['message']=err.message;
                }else{
                    if( rowCount <= 0 && /^EXEC/gi.test(query) === false){
                        result_data['status']=false;
                        result_data['message']='not found : '+query;
                    }
                }
                try {
                    await Promise.all(rows)
                    .then(data => {
                        try {
                            if (data.length <= 0) {
                                result_data['status'] = false;
                                result_data['data'] = [];
                            } else {
                                Object.keys(data).map(item => {
                                    jsonString += data[item][0]['value'];
                                })
                                // Compatibility layer (For SQL Server v12 and below)
                                if (!isJsonString(jsonString)) {
                                    let _raw = rows;
                                    jsonString = JSON.stringify(_raw.map(_row => {
                                        let _a = {};
                                        _row.forEach(c => {
                                            _a[c.metadata.colName] = c.value;
                                        });
                                        return _a;
                                    }));
                                }
                                // data.forEach(item => {
                                //     jsonString += item[0]['value'];
                                // });
                                result_data['status'] = true;
                                result_data['data'] = JSON.parse(jsonString);
                            }
                            resolve(result_data);
                        } catch (e) {
                            console.log(new Date() +" ERROR 2: failed connect DB");
                            resolve(result_data);
                        }
                    });
                } catch (e) {
                    console.log(e);
                    console.log(new Date() +" ERROR 1: failed connect DB");
                    resolve(result_data);
                }
            })
        });
        try {
            connect.execSql(request);
        } catch (error) {
            console.log(new Date() +" ERROR 00: failed connect DB");
        }
    });
}

function updateData(connect, query) {
    return new Promise((resolve) => {
        let result_data = {
            "message": "",
            "status": false,
            "data": []
        }

        let request = new Request(query,function (err, rowCount) {
            connect.close();
            connect.on("end", async function () {
                if (err) {
                    result_data['message']=err.message;
                } else {
                    result_data['status']=true;
                    result_data['data']=rowCount;
                }
                resolve(result_data);
            })
            });
            try {
                connect.execSql(request);
            } catch (error) {
                console.log(new Date() +" ERROR 00: failed connect DB");
            }
    });
}

function deleteData(connect, query) {
    return new Promise((resolve) => {
        let result_data = {
            "message": "",
            "status": false,
            "data": []
        }
        let request = new Request(query,function(err, rowCount) {
            connect.close();
            connect.on("end", async function () {
                if (err) {
                    result_data['message']=err.message;
                } else {
                    result_data['status']=true;
                    result_data['data']=rowCount;
                }
                resolve(result_data); 
            })
 
        });
        try {
            connect.execSql(request);
        } catch (error) {
            console.log(new Date() +" ERROR 00: failed connect DB");
        }
    });
}

function insertData(connect, query) {
    return new Promise((resolve) => {
        let result_data = {
            "message": "",
            "status": false,
            "data": []
        }
        let request = new Request(query,function(err, rowCount, rows) {
            connect.close();
            connect.on("end", async function () {
                if (err) {
                    result_data['message']=err.message;
                } else {
                    result_data['status']=true;
                    // result_data['data']=rowCount;
                    if (rows.length > 0) {
                        result_data['data'] = rows[0][0]['value'];
                    } else {
                        result_data['data'] = rows;
                    }
                }
                resolve(result_data);
            })
        });
        try {
            connect.execSql(request);
        } catch (error) {
            console.log(new Date() +" ERROR 00: failed connect DB");
        }    });
}

function makeConnection(connect) {
    return new Promise((resolve, reject) => {
        (async () => {
            try {
                connect.on('connect', function (err) {
                    if (err) return reject(err);
                    resolve(connect);
                });
            } catch (err) {
                console.log(new Date() +" ERROR 0: failed connect DB");
                console.log(err)
            }
        })()
    });
}

function parsePaginate(queryString = '', options = {}, defaultSortColumn = 'id') {
    let dataset = {
        page: 0,
        perPage: 0,
        sortBy: '',
        sortDir: '',
        filter: {},
        rawFilter: '',
    };
    dataset = {
        page: options._page && parseInt(options._page, 10) > 1 ? options._page : 1,
        perPage: options._perPage || 25,
        sortBy: options._sortBy || defaultSortColumn,
        sortDir: options._sortDir || null,
        filter: null,
        rawFilter: options._rawFilter || null,
    };
    Object.keys(options).filter(v => v.indexOf('_like') >= 0).forEach(k => {
        if (!dataset.filter) {
            dataset.filter = {};
        }
        dataset.filter[k.substr(0, k.indexOf('_like'))] = options[k];
    });
    // PAGINATION
    let withPagination = [queryString];
    // 1. Parse filter request
    let code = parseInt([dataset.rawFilter, dataset.filter].map(v => v && true ? '1' : '0').join(''), 2);
    switch (code) {
        case 3: { 
            withPagination.push(`WHERE ${dataset.rawFilter}`);
            if (dataset.filter) {
                withPagination.push(...(Object.keys(dataset.filter).map((k,i) => `${ i == 0? 'AND'  : 'OR' } ${k} like '%${dataset.filter[k]}%'`)));
            }
            break;
         }
        case 2: { 
            withPagination.push(`WHERE ${dataset.rawFilter}`);
            break;
         }
        case 1: { 
            if (dataset.filter) {
                for (let k in dataset.filter) {
                    withPagination.push(`${withPagination.length > 1 ? 'AND' : 'WHERE'} ${k} like '%${dataset.filter[k]}%'`);
                }
            }
            break;
         }
    }
    // 2. Parse sorting with direction
    if (dataset.sortBy) {
        let direction = dataset.sortDir === 'DESC' ? 'DESC' : 'ASC';
        withPagination.push(`ORDER BY ${dataset.sortBy} ${direction}`);
    }
    // 3. Parse paging
    withPagination.push(`OFFSET ${(dataset.page - 1) * dataset.perPage} ROWS`, `FETCH NEXT ${dataset.perPage} ROWS ONLY`);
    console.log('Paginator:', withPagination.join(' '));   //liat cinsole
    return withPagination.join(' ');
}
function countPaginate(queryString = '', options = {}) {
    let dataset = {
        page: 0,
        perPage: 0,
        filter: {},
        rawFilter: '',
    };
    dataset = {
        page: options._page && parseInt(options._page, 10) > 1 ? options._page : 1,
        perPage: options._perPage || 25,
        filter: null,
        rawFilter: options._rawFilter || null,
    };
    Object.keys(options).filter(v => v.indexOf('_like') >= 0).forEach(k => {
        if (!dataset.filter) {
            dataset.filter = {};
        }
        dataset.filter[k.substr(0, k.indexOf('_like'))] = options[k];
    });
    // PAGINATION
    let withPagination = [queryString.replace(/SELECT(.|\n)*?FROM/i, 'SELECT count(*) as total FROM')];
    // 1. Parse filter request
    let code = parseInt([dataset.rawFilter, dataset.filter].map(v => v && true ? '1' : '0').join(''), 2);
    switch (code) {
        case 3: { 
            withPagination.push(`WHERE ${dataset.rawFilter}`);
            if (dataset.filter) {
                withPagination.push(...(Object.keys(dataset.filter).map((k,i) => `${ i == 0? 'AND'  : 'OR' } ${k} like '%${dataset.filter[k]}%'`)));
            }
            break;
         }
        case 2: { 
            withPagination.push(`WHERE ${dataset.rawFilter}`);
            break;
         }
        case 1: { 
            if (dataset.filter) {
                for (let k in dataset.filter) {
                    withPagination.push(`${withPagination.length > 1 ? 'AND' : 'WHERE'} ${k} like '%${dataset.filter[k]}%'`);
                }
            }
            break;
         }
    }
    // console.log('Options:', dataset);
    console.log('Counter:', withPagination.join(' '));
    return withPagination.join(' ');
}
function IsJson(val) {
    return val instanceof Array || val instanceof Object ? true : false;
}
/**
 * 
 * @param {string} str 
 */
function isJsonString(str) {
    str = (typeof str) !== 'string' ? JSON.stringify(str) : str;
    try {
        str = JSON.parse(str);
    } catch (e) {
        return false;
    }
    return (typeof str) === 'object' && str !== null
}
function newConfig(newConfig){
    let nt = JSON.parse(JSON.stringify(config));
    Object.keys(nt).map(item => {
        if(IsJson(newConfig[item])){
          Object.keys(nt[item]).map(item2=> {
              if(typeof newConfig[item][item2] !== 'undefined'){
                  nt[item][item2]=newConfig[item][item2];
              }
          })
        }else{
          if(typeof newConfig[item] !== 'undefined'){
              nt[item]=newConfig[item];
          }
        }
    });
    return nt;
}
/**
 * Paginate query
 * @param {string} queryString Target query
 * @param {{ _page: number; _perPage: number; _sortBy: string; _sortDir: 'ASC' | 'DESC'; filter: {[key: string]: any}; _rawFilter?: string }} options Query Object (e.g. req.query). Be very cautious when using _rawFilter!
 * @param {string} defaultSortColumn Default sorting column, default to "id"
 * @returns {Promise<{
 *  page: T[];
 *  total: number;
 * }>} Data of paginated result and total count of overall unpaginated rows
 */
methods.paginate = async function paginateQuery(queryString = '', options = {}, defaultSortColumn = 'id', additionalQuery = 'for json path', NewConnect=null) {
    let r = {
        page: [],
        total: 0,
    };
    // console.log('Additional:', additionalQuery);
    let result = await Promise.all([
        parsePaginate(queryString, options, defaultSortColumn),
        countPaginate(queryString, options),
    ].map(v => {
        var connect;
        if(NewConnect==null){
            connect = new Connection(config);
        }else{
            connect = new Connection(newConfig(NewConnect));
        }
        return makeConnection(connect).then(() => readData(connect, v, additionalQuery));
    }));
    // console.log('Internal:', result);
    if (result && result[0] && result[1] && result[0].status && result[1].status) {
        r = {
            page: result[0].data,
            total: result[1].data[0].total,
        };
        return r;
    } else {
        const e = new Error(result[0].message);
        e['code'] = 404;
        throw e;
    }
}

methods.read = function (query,for1=null,NewConnect=null) {
    var connect;
    if(IsJson(for1)){
        connect = new Connection(newConfig(for1));
        for1 = 'FOR JSON AUTO';
    }else  if(IsJson(NewConnect)){
        connect = new Connection(newConfig(NewConnect));
    }else{
        connect  = new Connection(config);
    }
    // console.log(newConfig);

    return makeConnection(connect).then(function () {
        return readData(connect, query,for1).then(function (result) {
            return result;
        })
    }).catch(function (err) {
        return err;
    });
}

methods.update = function (query,NewConnect=null) {
    var connect;
    if(NewConnect==null){
        connect = new Connection(config);
    }else{
        connect = new Connection(newConfig(NewConnect));
    }
    return makeConnection(connect).then(function () {
        return updateData(connect, query).then(function (result) {
            return result;
        })
    }).catch(function (err) {
        return err;
    });
}

methods.delete = function (query,NewConnect=null) {
    var connect;
    if(NewConnect==null){
        connect = new Connection(config);
    }else{
        connect = new Connection(newConfig(NewConnect));
    }
    return makeConnection(connect).then(function () {
        return deleteData(connect, query).then(function (result) {
            return result;
        })
    }).catch(function (err) {
        return err;
    });
}

methods.insert = function (query,NewConnect=null) {
    var connect;
    if(NewConnect==null){
        connect = new Connection(config);
    }else{
        connect = new Connection(newConfig(NewConnect));
    }
    return makeConnection(connect).then(function () {
        return insertData(connect, query).then(function (result) {
            return result;
        })
    }).catch(function (err) {
        return err;
    });
}

module.exports = methods;
