'use strict';
let methods = {};
const modules = require('./_module');
const crypto = require('./_crypto');

methods.authentication = function (active = true) {
    let data = {
        "message": "",
        "status": false,
        "data": [],
        "response_code": 200
    }
    return async function (req, res, next) {
        let encrypt = false;
        encrypt = res.req.originalUrl.split('/').includes('v2');
        if (!active) {
            if (encrypt && req.body && req.body.data) {
                try {
                    let result = await crypto.decrypt(req.body.data);
                    req.body = result;
                    next();
                } catch (error) {
                    data['status'] = false;
                    data['message'] = "xxxxxxxencryptxxxxxxx";
                    data['response_code'] = 401;
                    modules.returnend(res, data);
                }
            } else {
                next();
            }
        } else {
            let token = req.headers['x-access-token'] || req.headers['authorization'];
            if (typeof (token) !== "undefined") {
                if (token.startsWith('Bearer ')) {
                    token = token.slice(7, token.length);
                    try {
                        let decoded = await verifyAsync(token);
                        res.locals.DataUser = decoded;
                        if (typeof (decoded['key_token']) !== "undefined") {
                            if (encrypt && req.body.data) {
                                try {
                                    let result = await crypto.decrypt(req.body.data);
                                    req.body = result;
                                    next();
                                } catch (e0) {
                                    data['status'] = false;
                                    data['message'] = "xxxxxxxencryptxxxxxxx";
                                    data['response_code'] = 401;
                                    modules.returnend(res, data);
                                }
                            } else {
                                next();
                            }
                        } else {
                            if (encrypt && req.body.data) {
                                try {
                                    let result = await crypto.decrypt(req.body.data);
                                    req.body = result;
                                    next();
                                } catch (e3) {
                                    data['status'] = false;
                                    data['message'] = "xxxxxxxencryptxxxxxxx";
                                    data['response_code'] = 401;
                                    modules.returnend(res, data);
                                }
                            } else {
                                next();
                            }
                        }
                    } catch (e2) {
                        data['status'] = false;
                        data['message'] = "sorry you don't have access";
                        data['response_code'] = 401;
                        modules.returnend(res, data);
                    }
                } else {
                    data['status'] = false;
                    data['message'] = "sorry you don't have access";
                    data['response_code'] = 401;
                    modules.returnend(res, data);
                }
            } else {
                data['status'] = false;
                data['message'] = "sorry you don't have access";
                data['response_code'] = 401;
                modules.returnend(res, data);
            }
        }
    }
};
methods.response = {
    "message": "",
    "status": true,
    "data": [],
    "response_code": 200
  }
module.exports = methods;