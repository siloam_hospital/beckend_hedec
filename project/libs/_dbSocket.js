'use strict';
let methods = {};
const jwtAuth = require('socketio-jwt-auth');
const socket = require('socket.io');

let io = socket();

methods.sendBroadcast = (message, event = 'broadcast') => {
  io.of('/').emit(event, message);
}

/**
 * 
 * @param {string} employeeID 
 * @param {string} event 
 * @param {*} message 
 */
methods.sendTo = (employeeID, event, message) => {
  io.to(`employeeId:${employeeID}`).emit(event, message);
}

/**
 * 
 * @param {string} userLevel 
 * @param {string} event 
 * @param {*} message 
 */
methods.sendToLevel = (userLevel, event, message) => {
  io.to(`userLevel:${userLevel}`).emit(event, message);
};

/**
 * 
 * @param {string} satker 
 * @param {string} event 
 * @param {*} message 
 */
methods.sendToSatker = (satker, event, message) => {
  io.to(`satker:${satker}`).emit(event, message);
}

/**
 * 
 * @param {string} namespace 
 * @param {string} rooms 
 * @param {string} event 
 * @param {*} message 
 */
methods.send = (namespace, rooms, event, message) => {
  io.of(namespace).to(rooms).emit(event, message);
};

methods.addOnConnect = (listener) => {
  io.sockets.prependListener('connection', listener);
};

/**
 * 
 * @param {SocketIO.Server} _io 
 */
methods.use = function (_io) {
  io = _io;
  io.use(jwtAuth.authenticate({
    secret: process.env.jwt_token,
    succeedWithoutToken: false,
  }, function (payload, done) {
    if (payload) {
      return done(null, payload);
    } else {
      return done(null, false);
    }
  }));

  io.sockets.on('connection', function (socket) {
    // console.log('User:', socket.request.user);
    try {
      if (socket.request.user) {
        socket.join(`employeeId:${socket.request.user.employeeID}`);
        socket.join(`userLevel:${socket.request.user.user_level}`);
        if (socket.request.user.position) {
          socket.join(`satker:${socket.request.user.position.code_satker}`);
        }
      }
    } catch (e) {
      console.error('Auth Error:', e);
    }
    io.clients((error, clients) => {
      if (error) throw error;
      console.log("connection:", { clients, count: clients.length });
    });

    socket.on('join', (room) => {
      socket.join(room);
      socket.emit('servermessage', { joinTo: room });
    });

    socket.on('gpsinput', (avlData) => {
      io.of('/').to('/mapo/realtime').emit('gpsdata', avlData);
    });

    socket.on('fmsinput', (fmsData) => {
      io.of('/').to('/elektrifikasi/realtime').emit('data', fmsData);
    });

    socket.on('__legacy', (d) => {
      if (d && d.ns && d.room && d.e && d.data) {
        io.of(d.ns).to(d.room).emit(d.e, d.data);
      }
    });

    socket.on('dashdiv_input', (avlData) => {
      io.of('/').to('/auth/realtime').emit('dashdiv', avlData);
    });

    socket.on('disconnect', function (...args) {
      console.log('disconnection:', args);
      io.clients((error, clients) => {
        if (error) throw error;
        console.log("disconnect", { clients, count: clients.length });
        // console.log(clients);
      });
    });

    setInterval(() => {
      io.emit('servertime', (new Date()).toISOString());
    }, 500);
  })
};

module.exports = methods;
