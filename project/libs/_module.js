'use strict';
let methods = {};
const fs = require('fs');
const crypto = require('./_crypto'); 

methods.generateConfig = function (tableName = null) {
    return {
        host: process.env.DB_Host,
        port: process.env.DB_Port,
        user: process.env.DB_user,
        password: process.env.DB_Password,
        database: tableName!=null?tableName:process.env.DB_Name,
    };
};

methods.encryptData = function (res, data = null) {
    let encrypt = false;
    res.req.originalUrl.split("/").findIndex(item => {
        if (item === "v2") { //berlaku res data encrypt
            encrypt = true;
            return true;
        } else if (item === "v1") {
            encrypt = false;
        }
    })

    if (data != null) {
        if (encrypt) {
            return crypto.encryptSync(data);
        } else if (encrypt === false) {
            return JSON.stringify({
                "message": data['message'],
                "status": data['status'],
                "data": data['data'],
            });
        } else {
            return JSON.stringify({
                "message": "not allowed",
                "status": false,
                "data": [],
            });
        }
    } else {
        return data;
    }
}

methods.returnend = async function (res, data = null) {
    res.setHeader('X-Powered-By', process.env.Service);
    res.setHeader("Content-Type", "application/json;charset=utf-8");

    let encrypt = null;
    res.req.originalUrl.split("/").findIndex(item => {
        if (item === "v2") { //berlaku res data encrypt
            encrypt = true;
            return true;
        } else if (item === "v1") {
            encrypt = false;
        }
    })
    if (data != null) {
        if (encrypt) {
            crypto.encrypt(data).then(function (result) {
                res.writeHead(data['response_code']);
                res.end(result);
            });
        } else if (encrypt === false) {
            res.status(data['response_code']).json({
                "message": data['message'],
                "status": data['status'],
                "data": data['data'],
            });
        } else {
            res.status(data['response_code']).json({
                "message": "not allowed",
                "status": false,
                "data": [],
            });
        }
    }
    // res.setHeader("Content-Encoding", "gzip");
    // const buf = Buffer.from(JSON.stringify(data), 'utf-8');
    // zlib.gzip(buf, function (_, result) {
    //     res.end(result);
    // });
};
methods.returnendFile = async function (res, path, filename) {
    fs.exists(path, function (exists) {
        if (exists) {
            res.writeHead(200, {
                "Content-Type": "application/octet-stream",
                "Content-Disposition": "attachment; filename=" + filename
            });
            fs.createReadStream(path).pipe(res);
        } else {
            res.writeHead(400, {
                "Content-Type": "text/plain"
            });
            res.end("ERROR File does not exist");
        }
    });

};

methods.batchInsert = function (dataset, tableName) {
let keySet = new Set(dataset.map(v => Object.keys(v)).reduce((p, c) => p.concat(c), []));
let flattenKey = [...keySet.values()];
return `INSERT INTO ${tableName}(${flattenKey.join(', ')}) VALUES ${dataset.map(v => `(${flattenKey.map(k => {
        if (v[k] === null || v[k] === undefined) {
            return 'null';
        } else if ((typeof v[k]) === 'number') {
            return v[k].toString();
        } else {
            return `'${v[k]}'`;
}
}).join(',')
})
`).join(',')}`;
}

methods.normalizeQuery = function (query){
    return query.split("'getDate()'").join("getDate()").split("'null'").join("null");
}

methods.merge = function (dataset,tableName,identity){
    let keySet = new Set(dataset.map(v=> Object.keys(v)).reduce((p,c)=>p.concat(c),[]));
    let flattenKey = [...keySet.values()];
    let filterKey = [...flattenKey].filter(a=>a != 'id');
    let unionQuery = dataset.map(v=>`UNION SELECT ${
        flattenKey.map(k=>{
            if (v[k] === null || v[k] === undefined) {
                return 'null';
            } else if ((typeof v[k]) === 'number') {
                return v[k].toString();
            } else {
                return `'${v[k]}'`;
            }
        }).join(', ')
    } `).join('\n');
    let updateQuery = `update set ${filterKey.map(k=>`A.${['created_by','created_time'].some(z=>z==k)?k.split('created_').join('modified_'):k} = B.${k}`).join(', ')}`;
    let insertQuery = `insert (${filterKey.map(k=>`${k}`).join(', ')})
    values (${filterKey.map(k=>`B.${k}`).join(', ')})
    `
    return `
    with B as (
        select top 0 ${flattenKey.join(', ')} from ${tableName} 
        ${unionQuery}
    )
    merge into ${tableName} A USING B ON A.${identity} = B.${identity}
    when matched then
    ${updateQuery}
    when not matched then
    ${insertQuery}
    output inserted.*;
    `
}

methods.validateEmail = function (email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
methods.generatekey = function () {
    let buffer = require('crypto').randomBytes(13);
    return buffer.toString('hex');
};
methods.unixTime = function (unixtime) {
    let u = new Date(unixtime * 1000);
    return u.getUTCFullYear() +
        '-' + ('0' + u.getUTCMonth()).slice(-2) +
        '-' + ('0' + u.getUTCDate()).slice(-2) +
        ' ' + ('0' + u.getUTCHours()).slice(-2) +
        ':' + ('0' + u.getUTCMinutes()).slice(-2) +
        ':' + ('0' + u.getUTCSeconds()).slice(-2) +
        '.' + (u.getUTCMilliseconds() / 1000).toFixed(3).slice(2, 5);
};
methods.whereBuilder = function (request, where) {
    let obj = request;
    let result = "";
    let objSize = 0;

    for (var o in obj) {
        if (o == where) {
            continue;
        } else {
            let q = "" + o + " = '" + obj[o] + "'";
            result = result + q;
            if (objSize < Object.keys(obj).length - 2) {
                result = result + ", ";
            }
        }
        objSize++;
    }

    // let result = JSON.stringify(where);
    // result = result.replace("{", '')
    // result = result.replace(":", " = ")
    return result;
}

methods.updateBuilder = function (request, table, whereParam, whereValue) {
    let obj = request;
    let result = [];


    for (var o in obj) {
        result.push(o + " = '" + obj[o] + "'");
    }

    let query = "UPDATE " + table + " SET " + result.join(', ') + " WHERE " + whereParam + " = '" + whereValue + "'";

    return query;
}

methods.updateBuilderMultiCondition = function (request, table, whereCondition) {
    let obj = request;
    let cond = whereCondition;
    let result = [];
    let condition = [];


    for (var o in obj) {
        result.push(o + " = '" + obj[o] + "'");
    }

    for (var i in cond) {
        if(cond[i] != 'undefined'){
            condition.push(i + " = '"+cond[i]+ "'");
        }else{
            condition.push(i + " is null ")
        }
    }

    let query = "UPDATE " + table + " SET " + result.join(', ') + " WHERE " + condition.join(' AND ');
    console.log(query)
    return query;
}

methods.insertBuilder = function (request, table) {
    let obj = request;
    let fields = [];
    let values = [];

    for (var o in obj) {
        if (obj[o] != null){
            fields.push(o);
            if ((typeof obj[o]) === 'number') {
                values.push(obj[o].toString());
            } else if ((typeof obj[o]) === 'boolean') {
                values.push(obj[o] ? 1 : 0);
            }else{
                values.push("'" + obj[o] + "'");
            }
        }else{
            //fields.push(o);
            //values.push( obj[o] );
        }
    }

    let query = "INSERT INTO " + table + " (" + fields.join(',') + ") VALUES (" + values.join(',') + ")";

    return query;
}

methods.insertBuilderReturnID = function (request, table, whereClause) {
    let obj = request;
    let fields = [];
    let values = [];

    for (var o in obj) {
        fields.push(o);
        values.push("'" + obj[o] + "'");
    }
    let queryWhere = ''
    if(whereClause!=null){
        queryWhere = whereClause;
    }

    let query = "INSERT INTO " + table + " (" + fields.join(',') + ") VALUES (" + values.join(',') + ") "+queryWhere+"; SELECT LAST_INSERT_ID();";

    return query;
}
methods.validateEmail = function (email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
module.exports = methods;