'use strict';
let methods = {};
const CryptoJS = require("crypto-js");
let cryptoKey = process.env.cryptoKey;
let cryptoiv = process.env.cryptoiv;

methods.encryptSync = (data = null) => {
    let now = new Date();
        let tamp_data = data;
        tamp_data['create'] = now;

        let key = CryptoJS.enc.Utf8.parse(cryptoKey);
        let iv = CryptoJS.enc.Utf8.parse(cryptoiv);
        let datastring = JSON.stringify(tamp_data);
        var ciphertext = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(datastring), key, {
            keySize: 128,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString();
        return ciphertext;
}

methods.encrypt = function (data = null) {
    return new Promise((resolve) => {
        let now = new Date();
        let tamp_data = data;
        tamp_data['create'] = now;

        let key = CryptoJS.enc.Utf8.parse(cryptoKey);
        let iv = CryptoJS.enc.Utf8.parse(cryptoiv);
        let datastring = JSON.stringify(tamp_data);
        var ciphertext = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(datastring), key, {
            keySize: 128,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        }).toString();
        return resolve(ciphertext);
    })
};

methods.decrypt = function (ciphertext) {
    return new Promise((resolve, reject) => {
        try {
            let key = CryptoJS.enc.Utf8.parse(cryptoKey);
            let iv = CryptoJS.enc.Utf8.parse(cryptoiv);
            let bytes = CryptoJS.AES.decrypt(ciphertext, key, {
                keySize: 128,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });
            resolve(JSON.parse(bytes.toString(CryptoJS.enc.Utf8)));
        } catch (e) {
            reject(e);
        }
    })
};
module.exports = methods;

