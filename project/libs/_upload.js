'use strict';
let methods = {};
const multer = require('multer');
const fs = require('fs');
const compress_images = require('compress-images');
const moveFile = require('move-file');

const storage = multer.diskStorage({
    destination: function (req, file, cb, res) {
        let folder = './public/uploads/';
        let source = './../../' + folder;
        try {
            fs.mkdirSync(folder);
        } catch (err) {
            if (err.code != 'EEXIST') {
                throw err;
            }
        }

        if (typeof (req.params.id) !== "undefined" && file.fieldname != 'profile') {
            source += req.params.id;
            folder += req.params.id;
            try {
                fs.mkdirSync(folder);
            } catch (err) {
                if (err.code != 'EEXIST') {
                    throw err;
                }
            }
        } else {
            source += 'public';
            folder += 'public';
            try {
                fs.mkdirSync(folder);
            } catch (err) {
                if (err.code != 'EEXIST') {
                    throw err;
                }
            }
        }

        if (file.fieldname === 'img') {
            source += '/images/';
            folder += '/images/';
        } else if (file.fieldname === 'file') {
            source += '/files/';
            folder += '/files/';
        }
        try {
            fs.mkdirSync(folder);
        } catch (err) {
            if (err.code != 'EEXIST') {
                throw err;
            }
        }
        file['folder'] = folder;
        file['url'] = folder.substr(8);
        cb(null, __dirname + source)
    },
    filename: function (req, file, cb, res) {
        let extArray = file.mimetype.split("/");
        let extension = extArray[extArray.length - 1];
        let name = "";

        if (file.fieldname === 'profile') {
            name = 'E' + req.params.id + '.gif'; //+extension;//'.gif';
            file['url'] += name;
            cb(null, name)
        } else {
            name = file.fieldname + '-' + Date.now() + '.' + extension;
            file['url'] += name;
            cb(null, name)
        }

    }
})

methods.compressIMG = function (file) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            (async () => {
                try {
                    await moveFile(file['path'], file['folder'] + '_temp/' + file['filename']);
                    compress_images("./" + file['folder'] + '_temp/' + file['filename'], file['folder'], {
                            compress_force: false,
                            statistic: false,
                            autoupdate: true
                        }, false, {
                            jpg: {
                                engine: "mozjpeg",
                                command: ["-quality", "60"]
                            }
                        }, {
                            png: {
                                engine: "pngquant",
                                command: ["--quality=20-50"]
                            }
                        }, {
                            svg: {
                                engine: "svgo",
                                command: "--multipass"
                            }
                        }, {
                            gif: {
                                engine: false,
                                command: false
                            }
                        },
                        //  {
                        //         gif: {
                        //             engine: "gifsicle",
                        //             command: ["--colors", "64", "--use-col=web"]
                        //         }
                        //     },
                        async function (err, completed) {
                            if (completed === true) {
                                fs.unlink("./" + file['folder'] + '_temp/' + file['filename'], function (err) {})
                                resolve(completed);
                            } else {
                                // console.log("coba dei ok mamang");
                                await moveFile(file['folder'] + '_temp/' + file['filename'], file['folder'] + file['filename']);
                                // reject(err);
                            }
                        });
                } catch (error) {
                    // console.log("ok mamang");
                    // console.log(error);
                    await moveFile(file['folder'] + '_temp/' + file['filename'], file['folder'] + file['filename']);
                }
            })();
        }, 500)
    })
}
methods.removefile = function (files) {
    return new Promise((resolve) => {
        let data = {
            "message": "",
            "status": true,
            "data": [],
        }
        let i = 0;
        Object.keys(files).map(key => {
            try {
                fs.unlink(files[key]['folder'] + files[key]['filename'], function (err) {})
                fs.unlink(files[key]['folder'] + "_temp/" + files[key]['filename'], function (err) {})
                i = i + 1;
            } catch (error) {
                data['status'] = false;
            }
        })
        data['message'] = "Remove " + i + " files success";
        resolve(data);
    })
}

methods.uploadFile = multer({
    storage: storage
})

methods.uploadImages = multer({
    storage: storage
}).array('img', process.env.maxFileUpload);

methods.uploadFiles = multer({
    storage: storage
}).array('file', process.env.maxFileUpload);

module.exports = methods;