const express = require('express');
const app = express.Router();
var jwt = require('jsonwebtoken');
const {returnend,generateConfig,insertBuilder} = require('./libs/_module');
const { authentication,response } = require('./libs/_auth');
const { MyDB } = require('./libs/MyDb');
const db = new MyDB(generateConfig());

app.use(authentication(false));

app.get('/hospitals', function (req, res) {
  let query = `select * from hospitals`;
  db.execute(query).then(function (result) {
    response['data']=result
  }).catch((err) => {
    response['message']=err
    response['status']=false;
  }).finally(() => {
    returnend(res,response);
  });
});

app.post('/health', async function (req, res) {
 let result_personal = await db.execute(insertBuilder(req.body.personal,'personal'))
 if(typeof result_personal['insertId'] !='undefined'){
    req.body.symptoms['id_personal']=result_personal['insertId'];
    let result_symptoms = await db.execute(insertBuilder(req.body.symptoms,'symptoms'))
    if(typeof result_symptoms['insertId'] =='undefined'){
      response['status']=false;
    }else{
      response['data']=result_personal['insertId'];
    }
    returnend(res,response);
 }else{
  response['status']=false;
  returnend(res,response);
 }
});

app.post('/personal', async function (req, res) {
  if(req.body.complete_id){
    let query = `select personal.name, personal.gender, personal.email, personal.phone, personal.createdAt, personal.date_birth,hospitals.name as hospital,symptoms.reactive_result,symptoms.fever,symptoms.breathing,symptoms.cought,symptoms.runny_nose,symptoms.smell,symptoms.taste,symptoms.aches,symptoms.vomiting,
    symptoms.live_someone,symptoms.close_contact,symptoms.with_someone,symptoms.close_contact_recovered,symptoms.treating_patients
    from personal
    left join symptoms on personal.id = symptoms.id_personal
    left join hospitals on hospitals.id = personal.id_hospital
    where personal.id=${Number(req.body.complete_id)} and personal.isDeleted=0`
    try {
      let result = await db.execute({ sql:query, rowsAsArray: false })
      response['data'] = result;
      response['status'] = true;
    } catch (error) {
      response['message'] = error;
      response['status'] = true;
    } finally {
      returnend(res, response);
    }
  }else{
    response['message'] = 'incomplete data';
    response['status'] = false;
    returnend(res, response);
  }
})
app.get('/who', function (req, res) {
  response['message']=process.env.Service;
  returnend(res, response);
});
module.exports = app;