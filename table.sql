-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 25, 2021 at 09:36 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siloam`
--

-- --------------------------------------------------------

--
-- Table structure for table `hospitals`
--

CREATE TABLE `hospitals` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `isActive` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime DEFAULT current_timestamp(),
  `updatedAt` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hospitals`
--

INSERT INTO `hospitals` (`id`, `name`, `isDeleted`, `isActive`, `createdAt`, `updatedAt`) VALUES
(1, 'BIMC Hospital Kuta', 0, 1, '2021-08-25 00:03:50', '2021-08-25 00:04:01'),
(2, 'BIMC Hospital Nusa Dua', 0, 1, '2021-08-25 00:05:23', NULL),
(3, 'MRCCC Siloam Hospitals Semanggi', 0, 1, '2021-08-25 00:05:34', NULL),
(4, 'RSU Syubbanul Wathon', 0, 1, '2021-08-25 00:05:48', NULL),
(5, 'Rumah Sakit Umum Siloam Kelapa Dua', 0, 1, '2021-08-25 00:05:58', NULL),
(6, 'Rumah Sakit Umum Siloam Lippo Village', 0, 1, '2021-08-25 00:06:08', NULL),
(7, 'Siloam Hospitals Balikpapan', 0, 1, '2021-08-25 00:06:20', NULL),
(8, 'Siloam Hospitals Bangka Belitung', 0, 1, '2021-08-25 00:06:32', NULL),
(9, 'Siloam Hospitals Bogor', 0, 1, '2021-08-25 00:06:32', NULL),
(10, 'Siloam Hospitals Buton', 0, 1, '2021-08-25 00:06:48', NULL),
(11, 'Siloam Hospitals Cinere', 0, 1, '2021-08-25 00:08:06', NULL),
(12, 'Siloam Hospitals Cirebon', 0, 1, '2021-08-25 00:08:06', NULL),
(13, 'Siloam Hospitals Denpasar', 0, 1, '2021-08-25 00:08:06', NULL),
(14, 'Siloam Hospitals Jambi', 0, 1, '2021-08-25 00:08:06', NULL),
(15, 'Siloam Hospitals Kebon Jeruk', 0, 1, '2021-08-25 00:08:06', NULL),
(16, 'Siloam Hospitals Lippo Cikarang', 0, 1, '2021-08-25 00:08:06', NULL),
(17, 'Siloam Hospitals Lippo Village', 0, 1, '2021-08-25 00:08:06', NULL),
(18, 'Siloam Hospitals Lubuk Linggau', 0, 1, '2021-08-25 00:08:06', NULL),
(19, 'Siloam Hospitals Makassar', 0, 1, '2021-08-25 00:08:06', NULL),
(20, 'Siloam Hospitals Manado', 0, 1, '2021-08-25 00:08:42', NULL),
(21, 'Siloam Hospitals Mataram', 0, 1, '2021-08-25 00:08:42', NULL),
(22, 'Siloam Hospitals Medan', 0, 1, '2021-08-25 00:08:42', NULL),
(23, 'Siloam Hospitals Palangkaraya', 0, 1, '2021-08-25 00:08:42', NULL),
(24, 'Siloam Hospitals Semarang', 0, 1, '2021-08-25 00:09:21', NULL),
(25, 'Siloam Hospitals Surabaya', 0, 1, '2021-08-25 00:09:21', NULL),
(26, 'Siloam Hospitals TB Simatupang', 0, 1, '2021-08-25 00:09:21', NULL),
(27, 'Siloam Hospitals Yogyakarta', 0, 1, '2021-08-25 00:09:21', NULL),
(28, 'Siloam Sriwijaya Palembang', 0, 1, '2021-08-25 00:09:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personal`
--

CREATE TABLE `personal` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `email` varchar(320) NOT NULL,
  `phone` int(25) NOT NULL,
  `id_hospital` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `isActive` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime DEFAULT current_timestamp(),
  `updatedAt` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `date_birth` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `symptoms`
--

CREATE TABLE `symptoms` (
  `id` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `reactive_result` tinyint(1) NOT NULL,
  `fever` tinyint(1) NOT NULL,
  `breathing` tinyint(1) NOT NULL,
  `cought` tinyint(1) NOT NULL,
  `runny_nose` tinyint(1) NOT NULL,
  `smell` tinyint(1) NOT NULL,
  `taste` tinyint(1) NOT NULL,
  `aches` tinyint(1) NOT NULL,
  `vomiting` tinyint(1) NOT NULL,
  `agreement` tinyint(1) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `isActive` tinyint(1) NOT NULL DEFAULT 1,
  `createdAt` datetime DEFAULT current_timestamp(),
  `updatedAt` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `live_someone` tinyint(1) DEFAULT NULL,
  `close_contact` tinyint(1) DEFAULT NULL,
  `with_someone` tinyint(1) DEFAULT NULL,
  `close_contact_recovered` tinyint(1) DEFAULT NULL,
  `treating_patients` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hospitals`
--
ALTER TABLE `hospitals`
  ADD UNIQUE KEY `hospitals_id_uindex` (`id`);

--
-- Indexes for table `personal`
--
ALTER TABLE `personal`
  ADD UNIQUE KEY `personal_id_uindex` (`id`),
  ADD KEY `personal_hospitals_id_fk` (`id_hospital`);

--
-- Indexes for table `symptoms`
--
ALTER TABLE `symptoms`
  ADD UNIQUE KEY `symptons_id_uindex` (`id`),
  ADD KEY `symptons_personal_id_fk` (`id_personal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hospitals`
--
ALTER TABLE `hospitals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `personal`
--
ALTER TABLE `personal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `symptoms`
--
ALTER TABLE `symptoms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `personal`
--
ALTER TABLE `personal`
  ADD CONSTRAINT `personal_hospitals_id_fk` FOREIGN KEY (`id_hospital`) REFERENCES `hospitals` (`id`);

--
-- Constraints for table `symptoms`
--
ALTER TABLE `symptoms`
  ADD CONSTRAINT `symptons_personal_id_fk` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
