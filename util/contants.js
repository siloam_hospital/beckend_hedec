module.exports = {
    PAGINATION: {
        LIMIT: 15
    },
    MOMENT: {
        DATE_TIME_FORMAT: 'YYYY-MM-DD HH:mm:ss'
    }
}