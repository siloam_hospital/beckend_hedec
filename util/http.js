const http = {
    /**
     * Generate object to http query params
     *
     * @param params
     * @returns {string}
     */
    http_build_query: (params) => {
        return '?'+Object.keys(params).map(key => {
            if (Array.isArray(params[key])) {
                return `${key}[]=${params[key]}`
            }

            return `${key}=${params[key]}`
        }).join('&')
    }
}

module.exports = http;