require('dotenv').config();
const express   = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const https = require('https');
const http = require('http');
// const request = require('request');
var logger = require('morgan');
const app = express();
const fs = require('fs');
const expressip = require('express-ip');
const compression = require('compression');
const kill = require('kill-port');
const helmet = require('helmet');
const useragent = require('express-useragent');

app.use(expressip().getIpInfoMiddleware);
app.use(useragent.express());

app.use(bodyParser.json({
  limit: '1mb',
  extended: true,
  strict: true,
}));
app.disable( 'x-powered-by' );
app.use(helmet.xssFilter())
app.use(helmet.frameguard())
app.use(helmet({
  frameguard: false
}))
app.use(helmet.noSniff())

app.use(compression());
app.use(expressip().getIpInfoMiddleware);


// var whitelist = ['http://localhost:4200', 'https://localhost:8080']
// var corsOptions = {
//   origin: function (origin, callback) {
//     console.log(origin);
    
//     if(process.env.Production==='true'){
//         if (whitelist.indexOf(origin) !== -1) {
//             callback(null, true)
//           } else {
//             //callback(new Error('Not allowed by CORS '+ origin))
//             callback("Not allowed !");
            
//           }
//     }else{
//         callback(null, true)
//     }    
//   }
// }

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.raw());
app.use(express.static(__dirname + '/public'));

if (process.env.Logger === 'true') {
    app.use(logger('dev'));
}

app.use('/api/:version/form-web', require("./project/form-web"));

//  Certificate 
let httpServer;
if(process.env.ssl=="true"){
  httpServer = https.createServer({
    key: fs.readFileSync(process.env.ssl_key),
    cert: fs.readFileSync(process.env.ssl_cert),
  }, app);
}else{
  httpServer = http.createServer(app);
}

// Handling unhandled handlers
app.use((req, _res, next) => {
  console.log(`Someone trying to access "${req.url}". I'm ignoring this Little Intruder ...`);
  next();
});

if(process.env.Production==='true'){
  kill(process.env.PORT_SERVER, 'tcp').then()
  .catch()
  setTimeout(() => {
    runServer();
  }, 1000)
}else{
  runServer();
}


async function runServer(){
  httpServer.listen(process.env.PORT_SERVER, function () {
    
      let port = httpServer.address().port;
      console.log("SERVER listening @ port: ", port);
      try {
        fs.mkdirSync('public/');
      } catch (err) {
          if (err.code != 'EEXIST') {
              throw err;
          }
      }
  });
}